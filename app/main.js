import Vue from 'vue'
import App from './App'
import config from 'common/config.js'
import http from 'common/api/index'
import local from 'common/api/local.js'
import common from 'common/api/common.js'

Vue.config.productionTip = false
Vue.prototype.$common=common
Vue.prototype.$http=http
Vue.prototype.$local=local
Vue.prototype.$config=config

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
