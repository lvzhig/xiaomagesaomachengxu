var main,receiver,filter;
var _codeQueryTag = false;  

function initScan(success) {
	let _this = this;  
	/* #ifdef APP-PLUS */  
	main = plus.android.runtimeMainActivity();//获取activity  
	/* var context = plus.android.importClass('android.content.Context'); */  
	var IntentFilter = plus.android.importClass('android.content.IntentFilter');  
	/* var Intent = plus.android.importClass('android.content.Intent'); */  
	filter = new IntentFilter();  
	filter.addAction("com.android.server.scannerservice.broadcast");  
	receiver = plus.android.implements('io.dcloud.feature.internal.reflect.BroadcastReceiver',{  
	onReceive : function(context, intent) {  
		plus.android.importClass(intent);  
		let code = intent.getStringExtra("scannerdata");  
		/*调用本页面某方法*/  
		_this.queryCode(code,success);  
	}});  
	/* #endif */  
}
function startScan(){  
	/* #ifdef APP-PLUS */  
	main.registerReceiver(receiver,filter);  
	/* #endif */  
}
function stopScan(){  
	/* #ifdef APP-PLUS */  
	main.unregisterReceiver(receiver);  
	/* #endif */  
}
function queryCode(code,success){  
//防重复  
	if(_codeQueryTag)return false;  
	_codeQueryTag = true;  
	setTimeout(function(){  
		_codeQueryTag = false;  
	},150);  
		//到这里扫描成功了，可以调用自己的业务逻辑，code就是扫描的结果  
	success(code)
}
export default {
	initScan,
	startScan,
	stopScan,
	queryCode
}