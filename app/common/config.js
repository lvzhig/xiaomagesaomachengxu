export default {
	//  填自己的开发者服务器接口地址 
	//url: "http://172.16.71.29:8020",
	url: "http://172.16.71.15:93",
	//  请求的参数   
	data: {},
	//  设置请求的 header，header 中不能设置 Referer。
	header: {
		'Content-type':'application/x-www-form-urlencoded'
	},
	//  （需大写）有效值：OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT  
	method: "POST",
	//  json    如果设为json，会尝试对返回的数据做一次 JSON.parse    
	dataType: "json",
	//  text    设置响应的数据类型。合法值：text、arraybuffer  1.7.0
	responseType: "text",
	//  收到开发者服务成功返回的回调函数    
	success() {},
	//  接口调用失败的回调函数 
	fail() {},
	//  接口调用结束的回调函数（调用成功、失败都会执行）
	complete(){},
	//sqlite options
	sqliteDb:{
		name:'MyAPPDb',
		path:'_doc/impulse.db'
	},
	SysConfig:{
		//APP当前登录状态
		APP_ISONLINE:true,
		//扫码记录
		TODAY_SCAN:[],
		//当前库
		inventlocdef_code:'',
		//当前货位
		wrhus_id:'',
		//当前仓库类型
		invent_flag:'',
		//当前登录人
		loginedUser:{},
		//当前权限
		currentPopedom:{},
		//一次上传的数据条数
		once_uploadCount:200,
		//离线扫码计数器
		counter_offline:0,
		//版本
		version:'1.0.7'
	},
	Popedom:{
		Role_29:{
			ALLOW_FLAG:'RK',
			//入库扫描
			ALLOW_STORAGE:true,
			//下线扫描
			ALLOW_OFFLINE:false,
			//出库扫描
			ALLOW_EX:true			
		},
		Role_28:{
			ALLOW_FLAG:'CJ',
			//入库扫描
			ALLOW_STORAGE:false,
			//下线扫描
			ALLOW_OFFLINE:true,
			//出库扫描
			ALLOW_EX:false
		}
	},
	msgs:{
		needwifi:'wifi未连接',
		needonline:'此操作需要在线登录',
		initOK:'本地数据初始化完成',
		nowrhus:'货位未查到',
		nodata:'无数据',
		ex_ok:'出库操作成功',
		dberror:'数据库打开失败',
		sync_OK:'数据同步完成',
		clear_Fail:'本地数据清理出错',
		no_popedom:'无权限',
		no_wrhus_popedom:'无操作当前货位权限',
		install_fail:'安装失败',
		update_fail:'更新失败',
		isNewest:'已经是最新版本了',
		isChecking:'更新检查中...',
		isDownloading:'正在下载中...',
		isConnected:'已链接到服务器',
		isSyncing:'数据同步中...',
		isSearching:'查找中...'
	}
}