import config from './config.js'
//打开
function openDB(success,fail){
	// console.log(config.sqliteDb.path)
	if(!isDbOpen()){
		/* #ifdef APP-PLUS */ 
		plus.sqlite.openDatabase({
			name:config.sqliteDb.name,
			path:config.sqliteDb.path,
			success:function(e){
				// console.log('openDatabase success!');
				if(success) success(e)
			},
			fail:function(e){
				// console.log('openDatabase failed: ' + JSON.stringify(e));
				if(fail) fail(e)
			}
		})
		/* #endif */
	} else {
		if(success) success()
	}
}
//判断数据库是否已打开
function isDbOpen(){
	/* #ifdef APP-PLUS */ 
	return plus.sqlite.isOpenDatabase({
		name:config.sqliteDb.name,
		path:config.sqliteDb.path
	});
	/* #endif */
}
//关闭数据库
function closeDB(success,fail){
	if(isDbOpen()){
		/* #ifdef APP-PLUS */ 
		plus.sqlite.closeDatabase({
			name:config.sqliteDb.name,
			success:success,
			fail:fail
		})
		/* #endif */
	} else {
		if(success) success();
	}
}
//事务
function transaction(operation,success,fail){
	/* #ifdef APP-PLUS */ 
	plus.sqlite.transaction({
		name:config.sqliteDb.name,
		operation:operation,
		success:success,
		fail:fail
	})
	/* #endif */
}

function transactionBegin(s,f){
	transaction("begin",s,f)
}
function transactionCommit(s,f){
	transaction("commit",s,f)
}
function transactionRollback(s,f){
	transaction("rollback",s,f)
}
function _exec(sql,success,fail){
	/* #ifdef APP-PLUS */ 
	plus.sqlite.executeSql({
		name:config.sqliteDb.name,
		sql:sql,
		success:success,
		fail:fail
	})
	/* #endif */
}
//执行SQL
function executeSQL(sql,useTran,success,fail,noclose){
	openDB(()=>{
		exec()
	},(e)=>{
		//uni.showToast(config.msgs.dberror)
		// console.log('')
		if(fail) fail(e)
	})	
	function exec(){
		if(useTran){
			transactionBegin(()=>{
				_exec(sql,(data)=>{
					if(success) 
						success(data)
					transactionCommit();
					if(!noclose)
						closeDB();
				},(e)=>{
					if(fail)
						fail(e);
					transactionRollback();
					if(!noclose)
						closeDB();
				})
			})
		} else {
			_exec(sql,(data)=>{
				if(success)
					success(data);
				if(!noclose)
					closeDB();
			},(e)=>{
				console.log(e);
				if(fail)
					fail(e);
				if(!noclose)
					closeDB();
			})
		}
	}	
}


function selectSQL(sql,success,fail){
	openDB(()=>{
		/* #ifdef APP-PLUS */ 
		plus.sqlite.selectSql({
			name:config.sqliteDb.name,
			sql:sql,
			success:function(data){
				if(success)
					success(data)
			},
			fail:function(e){
				if(fail)
					fail(e);
			}
		})
		/* #endif */
	},(e)=>{
		//uni.showToast(config.msgs.dberror)
		if(fail)
			fail(e);
	})
}
//插入表值数据
function insertData(tablename,tabledata,success,fail){
	openDB(()=>{
		insertTbSql(tablename,tabledata,success,fail);
	},(e)=>{
		//uni.showToast(config.msgs.dberror)
		if(fail)
			fail(e);
	})	
}
//插入表值数据
function insertTbSql(tablename,tabledata,success,fail){
	if(tabledata.length>0){
		for(var i=0;i<tabledata.length;i++){
			var insertSql = "";
			var textcl = "";
			var valuel = "";
			var jcList = [];
			for (var ls in tabledata[i]){
				var tName = JSON.stringify(ls).replace('"', "'").replace('"', "'");				 
				if (textcl == "") {
					textcl += tName;
					valuel += "'" + tabledata[i][ls] + "'";
				} else {
					textcl += "," + tName;
					valuel += ",'" + tabledata[i][ls] + "'";
				}
			}
			if(textcl!=""){
				insertSql = "insert into " + tablename + " (" + textcl + ") values (" + valuel + ")";
				executeSQL(insertSql,false,success,fail,true);
			}
		}
	}
}
//建表
function createTbSql(tablename, tabledata, success, fail){
	var tbsql = "";
	if (tabledata.length > 0) {
		for (var ls in tabledata[0]) {
			var tName = JSON.stringify(ls).replace('"', "'").replace('"', "'");
			if (tbsql == "") {
				tbsql += tName + " text null ";
			} else {
				tbsql += "," + tName + " text null ";
			}
		}
	}
	//console.log(tbsql)
	if (tbsql != "") {
		tbsql = "create table if not exists " + tablename + " (" + tbsql + ")"
		executeSQL(tbsql, false, success, fail);
	}
}
//删除表
function dropTable(tablename, success, fail){
	var sql = 'drop table if exists  ' + tablename;
	executeSQL(sql, false, success, fail);
}
//清表
function clearTable(tablename, success, fail){
	var sql = 'DELETE FROM ' + tablename;
	executeSQL(sql, false, success, fail);
}
export default {
	executeSQL,
	selectSQL,
	insertData,
	createTbSql,
	dropTable,
	clearTable
}