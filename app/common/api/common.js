import SqliteDb from '../sqliteDb.js'
import _config from '../config.js'

Date.prototype.Format=function(fmt){
		var o = {
		        "M+": this.getMonth() + 1,                 //月份
		        "d+": this.getDate(),                    //日
		        "h+": this.getHours(),                   //小时
		        "m+": this.getMinutes(),                 //分
		        "s+": this.getSeconds(),                 //秒
		        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
		        "S": this.getMilliseconds()             //毫秒
		    };
		    if (/(y+)/.test(fmt))
		        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		    for (var k in o)
		        if (new RegExp("(" + k + ")").test(fmt))
		            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		    return fmt;
}
export default {
	//登录成功写本地参数
	loginSuccess:(data)=>{
		_config.SysConfig.loginedUser=data.data;
		// console.log(data.data.roleid)
		if(data.data.roleid=='28'){
			_config.SysConfig.currentPopedom=_config.Popedom.Role_28;
		}
		else{
			_config.SysConfig.currentPopedom=_config.Popedom.Role_29;
		}
		uni.switchTab({
			url:'../index/index'
		})
	},
	//wifi状态下执行
	isWifiExec:(success,fail)=>{
		uni.getNetworkType({
			success:(res)=>{
				if(res.networkType=="wifi"){
					success();
				}
				else{
					fail();
				}
			}
		})
	},
	// guid:()=>{
	// 	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,                         
	// 	function(c) {
	// 		var r = Math.random() * 16 | 0,
	// 		v = c == 'x' ? r : (r & 0x3 | 0x8);
	// 		return v.toString(16);
	//   });
	// },
	// //格式化日期
	formatDtTime:(d,success)=>{
		 // var year = d.getFullYear();
		 // var month = d.getMonth() + 1;
		 // var date = d.getDate();
		 // var hour = d.getHours();
		 // var minute = d.getMinutes();
		 // var second = d.getSeconds();		 
		 // success(year + "/" + month + "/" + date + " " + hour + ":" + minute + ":" + second);//必须是‘/’格式 
		 success(new Date(d).Format("yyyy-MM-dd hh:mm:ss"));
	},
	//格式化输出数据
	formatResponseData:(code,msg,data)=>{
		return JSON.parse('{"code": "'+code+'","msg": "'+msg+'","data": '+JSON.stringify(data)+'}');
	},
	//验证是否具有操作指定库的权限
	checkPopedom(success){
		console.log(_config.SysConfig.currentPopedom.ALLOW_FLAG)
		console.log(_config.SysConfig.invent_flag)
		if(_config.SysConfig.currentPopedom.ALLOW_FLAG==_config.SysConfig.invent_flag){
			success()
		} else {
			// uni.showToast(_config.msgs.no_wrhus_popedom)
			uni.showToast({
				title:_config.msgs.no_wrhus_popedom,
				icon:'none',
				duration:1500
			})
		}
	},
	//蜂鸣
	playErrBeep(amount){
		if(amount==null|| amount==undefined) amount=1;
		switch(plus.os.name){
			case "iOS":
			if ( plus.device.model.indexOf("iPhone") >= 0 ) { //判断是否为iPhone
					plus.device.beep(amount);
					console.log("设备蜂鸣中...");
				} else {
					console.log("此设备不支持蜂鸣");
				}
			break;
			default:
			plus.device.beep(amount);
			console.log("设备蜂鸣中...");
			break;
		}
	},
	vibrate(){
		switch ( plus.os.name ) { //判断设备类型
		    case "iOS":
		            if ( plus.device.model.indexOf("iPhone") >= 0 ) { //判断是否为iPhone
		                plus.device.vibrate();
		                console.log("设备振动中...");
		            } else {
		                console.log("此设备不支持振动");
		            }
		    break;
		    default:
		        plus.device.vibrate();
		            console.log("设备振动中...");
		    break;
		}
	}
}