import SqliteDb from '../sqliteDb.js'
import comn from './common.js'

export default {	
	//本地登录接口
	login:(data,success,fail)=>{
		var _this=this;
		try{
			SqliteDb.selectSQL("select * from SyncUserBean where loginname='"+data.username+"' and usercode2='"+data.password+"'",(data)=>{
				if(data.length>0){					
					console.log(data[0])
					success(comn.formatResponseData('200','登录成功',data[0]))
				}
				else{
					fail(comn.formatResponseData('300','用户名或密码 \n 错误',{}))
				}
			},(e)=>{
				fail(comn.formatResponseData('300',JSON.stringify(e),{}))
			})
		} catch (e){
			fail(comn.formatResponseData('300',JSON.stringify(e),{}))
		}
	},
	//获取本地扫码数据
	scan:(data,success)=>{
		try{
			var sql="";
			switch(data.type){
				case "cargo":
					if(data.barcode.indexOf("--")>0){
						var wrhus_id=data.barcode.substring(0,data.barcode.indexOf("--"));
						var invent_code=data.barcode.substring(data.barcode.indexOf("--")+2);
						console.log(wrhus_id);
						console.log(invent_code);
						sql="select * from SyncInventBean where UPPER(invent_code)=UPPER('"+invent_code+"') and UPPER(wrhus_id)=UPPER('"+wrhus_id+"')";
					}
					else{					
						sql="select * from SyncInventBean where UPPER(invent_code)=UPPER('"+data.barcode+"')";
					}					
				break;
				default:
				break;
			}			 
			//本地查询
			if(sql!=""){
				SqliteDb.selectSQL(sql,(res)=>{
					console.log(sql);
					console.log(res);
					success(comn.formatResponseData("200","数据查询成功",JSON.stringify(res)));
				},(e)=>{
					fail(comn.formatResponseData("300","数据查询失败",{}));
				})
			}
			else{
				fail(comn.formatResponseData("300","无效的参数",{}));//console.log(e)
			}
		} catch (e){
			fail(comn.formatResponseData("300",JSON.stringify(e),{}));//console.log(e)
		}
	},
	//离线操作
	offlineOp:(data,success,fail)=>{
		var ScanHistoryBean="ScanHistoryBean";
		SqliteDb.insertData(ScanHistoryBean,data,()=>{
			var sql="select * from ScanHistoryBean order by CAST(counter as decimal) desc"
			SqliteDb.selectSQL(sql,(res)=>{
				success(comn.formatResponseData("200","数据查询成功",JSON.stringify(res)));
			},(e)=>{			
				fail(comn.formatResponseData("300","数据查询失败",{}));
			})
		},(e)=>{
			fail(JSON.stringify(e));
		})
	},
	//本地数据获取
	ScanHistory:(success,fail)=>{
		var sql="select * from ScanHistoryBean  order by CAST(counter as decimal) desc"
		SqliteDb.selectSQL(sql,(res)=>{
			success(comn.formatResponseData("200","数据查询成功",JSON.stringify(res)));
		},(e)=>{
			fail(comn.formatResponseData("300","数据查询失败",{}));
		})
	},
	//本地计数器计数
	ScanHistoryMaxCounter:(success,fail)=>{
		var sql="select ifnull(MAX(CAST(ifnull(counter,0) as decimal)),0) as counter from ScanHistoryBean";
		SqliteDb.selectSQL(sql,(res)=>{
			success(comn.formatResponseData("200","数据查询成功",JSON.stringify(res)));
		},(e)=>{
			fail(comn.formatResponseData("300","数据查询失败",{}));
		})
	},
	//清除本地数据
	clearScanHistory:(success,fail)=>{
		var ScanHistoryData=[{code:"xx",wrhus_id:"xx",inventlocdef_code:"xx",create_who:"xx",create_date:"",counter:"0"}]
		var ScanHistoryBean='ScanHistoryBean'
		SqliteDb.dropTable(ScanHistoryBean,()=>{},()=>{})
		SqliteDb.createTbSql(ScanHistoryBean,ScanHistoryData,()=>{
			console.log("ScanHistoryBean OK")
		},(e)=>{
			console.log(JSON.stringify(e))
		})
		SqliteDb.insertData(ScanHistoryBean,ScanHistoryData,()=>{
			console.log("localData ScanHistoryBean inited")
		},(e)=>{
			console.log(e);
		})
		SqliteDb.clearTable(ScanHistoryBean,()=>{			
			success()
		},(e)=>{
			fail(e)
		})
	},
	//同步本地数据
	 insertLocalData:(data,success,fail)=>{
		try{
			var SyncInventBean="SyncInventBean";
			var SyncUserBean="SyncUserBean";
			
			var inventData=data.invent
			if(inventData.length>0){				
				SqliteDb.dropTable(SyncInventBean,()=>{},()=>{})
				SqliteDb.createTbSql(SyncInventBean,inventData,()=>{
					console.log("SyncInventBean created")
				},(e)=>{
					throw e;					
				})
				SqliteDb.insertData(SyncInventBean,inventData,()=>{
					console.log("localData SyncInventBean sync ok")
				},(e)=>{
					throw e;
				})
			}
			var userData=data.user
			if(userData.length>0){
				SqliteDb.dropTable(SyncUserBean,()=>{},()=>{})
				SqliteDb.createTbSql(SyncUserBean,userData,()=>{
					console.log("SyncUserBean created")
				},(e)=>{
					throw e;
				})
				SqliteDb.insertData(SyncUserBean,userData,()=>{
					console.log("localData SyncUserBean sync ok")
				},(e)=>{
					throw e;
				})
			}
			success()
		}
		catch(e){
			fail(e)
		}		
	}
}