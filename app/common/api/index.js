import https from '../interface.js'

export default {		
	//Online登录
	login:(data)=>{		
		return https({
			url:'/api/token/login',
			data
		})
	},	
	//获取需同步的数据条数
	syncount:(data)=>{
		return https({
			url:'/api/sync/remote_count',
			data
		});
	},
	//同步数据
	syndata:(data)=>{
		return https({
			url:'/api/sync',
			data
		});
	},
	
	//扫码
	scan:(data)=>{
		return https({
			url:'/api/barcode/scan',
			data
		})		
	},
	//下线扫描
	offline:(data)=>{
		return https({
			url:'/api/barcode/off_line_product',
			data
		})	
	},
	//入库扫描
	storage:(data)=>{
		return https({
			url:'/api/barcode/storage',
			data
		})	
	},
	//出库扫描
	ex:(data)=>{
		return https({
			url:'/api/barcode/ex',
			data
		})	
	},
	//出库扫描明细
	exbox:(data)=>{
		return https({
			url:'/api/barcode/ex_box',
			data
		})
	},
	//扫描发货单
	salesend:(data)=>{
		return https({
			url:'/api/barcode/salesend',
			data
		})	
	},
	//扫描发货单-显示扫码明细
	salesendscan:(data)=>{
		return https({
			url:'/api/barcode/salesend_scan',
			data
		})
	},
	//检查更新
	update_v:(data)=>{
		return https({
			url:'/api/v1/version/check',
			data
		})
	}
}